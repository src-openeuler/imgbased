%global _configure ../configure


Name:           imgbased
Version:        1.2.24
Release:        1
Summary:        Tools to work with an image based rootfs

License:        GPLv2+
URL:            https://www.github.com/fabiand/imgbased
Source0:        http://resources.ovirt.org/pub/src/%{name}/%{name}-%{version}.tar.xz

BuildArch:      noarch


BuildRequires:       make
BuildRequires:       automake
BuildRequires:       autoconf
BuildRequires:       rpm-build
BuildRequires:       git
BuildRequires:       asciidoc
BuildRequires:       systemd-units
%if 0%{?with_python3}
Requires:            python%{python3_pkgversion}-imgbased
%else
Requires:            python-imgbased
%endif

Requires:            lvm2
Requires:            util-linux
Requires:            augeas
Requires:            rsync
Requires:            tar
Requires:            openscap-scanner
Requires:            grubby

%{!?_licensedir:%global license %%doc}

%description
This tool enforces a special usage pattern for LVM.
Basically this is about having read-only bases and writable
layers atop.

%package -n python%{python3_pkgversion}-imgbased
Summary: A python 3 module for imgbased
BuildRequires:       python%{python3_pkgversion}-devel
%if 0%{?with_check}
BuildRequires:       python%{python3_pkgversion}-pycodestyle
BuildRequires:       python%{python3_pkgversion}-pyflakes
BuildRequires:       python%{python3_pkgversion}-nose
BuildRequires:       python%{python3_pkgversion}-six
BuildRequires:       python%{python3_pkgversion}-systemd
%endif
Requires:       python%{python3_pkgversion}-systemd
Requires:       python%{python3_pkgversion}-rpm
Requires:       dnf-plugin-versionlock
Requires:       python%{python3_pkgversion}

%description -n python%{python3_pkgversion}-imgbased
python%{python3_pkgversion}-imgbased is a python 3 library to manage lvm layers


%prep
%setup -q

%build

mkdir py3 && pushd py3
%configure PYTHON="%{__python3}"
make %{?_smp_mflags}


%install
install -Dm 0644 src/plugin-yum/imgbased-persist.py \
                 %{buildroot}/%{_prefix}/lib/yum-plugins/imgbased-persist.py
install -Dm 0644 src/plugin-yum/imgbased-persist.conf \
                 %{buildroot}/%{_sysconfdir}/yum/pluginconf.d/imgbased-persist.conf
install -Dm 0644 data/imgbase-setup.service %{buildroot}%{_unitdir}/imgbase-setup.service
install -Dm 0444 data/imgbased-pool.profile %{buildroot}%{_sysconfdir}/lvm/profile/imgbased-pool.profile

make -C py3 install DESTDIR="%{buildroot}"



%files
%doc README.md
%license LICENSE
%{_sbindir}/imgbase
%{_datadir}/%{name}/hooks.d/
%{_mandir}/man8/imgbase.8*
/%{_docdir}/%{name}/*.asc
%{_unitdir}/imgbase-setup.service
%{_sysconfdir}/lvm/profile/imgbased-pool.profile

%{_sysconfdir}/yum/pluginconf.d/imgbased-persist.conf
%{_prefix}/lib/yum-plugins/imgbased-persist.py*

%files -n python%{python3_pkgversion}-imgbased
%doc README.md
%license LICENSE
%{python3_sitelib}/%{name}/


%changelog
* Tue Dec 10 2024 liyunqing <liyunqing@kylinos.cn> -1.2.24-1
- Update version to 1.2.24
  - Split /var/tmp to its own partition
  - copr: fix missing suffix in built rpms
  - Update the env. for vdsm-tool execution
  - Don't migrate SELinux binary policy, even if it was updated

* Mon Jul 05 2021 lijunwei <lijunwei@kylinos.cn> -1.2.19-1
- Init package
